require('dotenv').config();
const env = process.env.NODE_ENV;

const development = {
    port: process.env.DEV_APP_PORT,
    db_url: process.env.DEV_DB_URL,
    front_url: process.env.DEV_FRONT_ALLOWED_URL,
    secret: process.env.SECRET
};
const test = {
    port: process.env.TEST_APP_PORT,
    db_url: process.env.TEST_DB_URL,
    front_url: process.env.TEST_FRONT_ALLOWED_URL,
    secret: process.env.SECRET
};
const production = {
    port: process.env.PORT,
    db_url: process.env.PROD_DB_URL,
    front_url: process.env.PROD_FRONT_ALLOWED_URL,
    secret: process.env.SECRET
};

const config = {
    development,
    test,
    production
};

module.exports = config[env];
