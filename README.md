# RecettesFamilleClient

RecettesFamille is a family project of recipe website allowing you to publish your recipes and share them with other users.
You can browse the recipes with filters to find recipes from a chief you admire, or with specific ingredients. You can also post comments on recipes you loved !

Bonus for developer performance :
- The Admin status exists and can be granted by Ludo, or another admin
- I added a feedback page in the app, so any user can give a feedback (general purpose, design focused, bug reporting, ...)

![Recettes-Famille Preview](./doc_images/recettes-famille2.gif)


This Back end application enables the client app to communicate with a mongo-lab database containing three entities:
[Recipes] - [Users] - [Feedbacks]

# Go see it yourself !
The website is available at [this url](https://recettes-famille.herokuapp.com) ! (I chose to host it on a free service of Heroku, so the first attempt to reach the website might take 10/15sec).

# Run it and develop on your own

1) Make sure you have [Node and npm](https://nodejs.org/en/download/) installed
2) Then you just have to install all the required modules.
3) Then just run the `npm start` command

You can use it to settle your own service on the same level. MongoLab provides free noSQL hosting under 0.5GBB Storage.