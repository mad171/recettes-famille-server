/** MAIN EXPRESS MODULE RUNNIG THE BACK END  */

const express = require('express');

const path = require('path');
const logger = require('morgan');
const cors = require('cors');
const bodyParser = require('body-parser');
const createError = require('http-errors');

const indexRouter = require('./routes/index');
const usersRouter = require('./routes/users');
const recipesRouter = require('./routes/recipes');
const feedbackRouter = require('./routes/feedbacks');

const config = require('./config.js');
require('./models/User');
require('./config/passport');

const app = express();

// Instanciate a mongoose object connected to the database
const dbHost = config.db_url;
const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
mongoose.connect(dbHost)
  .then(() => console.log('[RecettesFamille server]: Mongoose DB connection successful *****'))
  .catch((err) => {
    console.error('Erreur connection to database --> ' + err);
    console.log(err);
  });


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');


// Add headers
app.use(function (req, res, next) {

  // Website you wish to allow to connect
  res.setHeader('Access-Control-Allow-Origin', config.front_url);

  // Request methods you wish to allow
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

  // Request headers you wish to allow
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With, content-type, Authorization');

  // Set to true if you need the website to include cookies in the requests sent
  // to the API (e.g. in case you use sessions)
  res.setHeader('Access-Control-Allow-Credentials', true);

  // Pass to next layer of middleware
  next();
});

// Setting the express middlewares
app.use(cors());
app.use(logger('dev'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// Adding routers
app.use('/', indexRouter);
app.use('/user', usersRouter);
app.use('/recipe', recipesRouter);
app.use('/feedback', feedbackRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
