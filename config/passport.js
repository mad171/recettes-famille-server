const mongoose = require('mongoose');
const passport = require('passport');
const LocalStrategy = require('passport-local');

const User = require('../models/User');

/** Passport strategy used for loging in */
passport.use(new LocalStrategy({
    usernameField: 'user[nickname]',
    passwordField: 'user[password]',
}, (nickname, password, done) => {
    User.findOne({ nickname })
        .then((user) => {
            if (!user || !user.validatePassword(password)) {
                return done(null, false, { errors: { 'nickname or password': 'is invalid' } });
            }
            return done(null, user);
        }).catch(done);
}));

module.exports = passport;