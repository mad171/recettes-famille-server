const mongoose = require('mongoose');

const RecipeSchema = new mongoose.Schema({
    recip_name: String,
    recip_id: Number,
    recip_author: String,
    recip_duration: Number,
    recip_level: String,
    recip_type: String,
    recip_N_eaters: Number,
    recip_ingredientList: [{ ingredient: String, quantity: String, unity: String }],
    recip_explanations: String,
    recip_comments: [{ nickname: String, comment: String, date: { type: Date, default: Date.now } }],
    updated_at: { type: Date, default: Date.now },
});

module.exports = mongoose.model('Recipe', RecipeSchema);