const mongoose = require('mongoose');

const FeedbackSchema = new mongoose.Schema({
    feedback_id: Number,
    category: String,
    nickname: String,
    title: String,
    content: String,
    date: { type: Date, default: Date.now },
});

module.exports = mongoose.model('Feedback', FeedbackSchema);