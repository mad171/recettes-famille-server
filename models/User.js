const mongoose = require('mongoose');
const crypto = require('crypto');
const jwt = require('jsonwebtoken');
const config = require('../config.js');

const { Schema } = mongoose;

const UsersSchema = new Schema({
    nickname: {
        type: String,
        unique: true,
        required: true
    },
    isAdmin: Boolean,
    hash: String,
    salt: String,
});

/** Set user password hashing it with crypto module */
UsersSchema.methods.setPassword = function (password) {
    this.salt = crypto.randomBytes(16).toString('hex');
    this.hash = crypto.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex');
};

/** Check weither and entered password corresponds to the user's password */
UsersSchema.methods.validatePassword = function (password) {
    const hash = crypto.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex');
    return this.hash === hash;
};

/** Generate a token with JWT module associated to theuser and set its expiration date */
UsersSchema.methods.generateJWT = function () {
    const today = new Date;
    const expirationDate = new Date(today);
    const tokenDurationInMinutes = 60 * 4;
    expirationDate.setDate(today.getDate() + (tokenDurationInMinutes / 24) / 60);
    return jwt.sign({
        nickname: this.nickname,
        id: this._id,
        exp: parseInt(expirationDate.getTime() / 1000, 10),
    }, config.secret);
}

/** Return an authentification Json  with a new token for the user */
UsersSchema.methods.toAuthJSON = function () {
    return {
        _id: this._id,
        nickname: this.nickname,
        isAdmin: this.isAdmin,
        token: this.generateJWT(),
    };
};

module.exports = mongoose.model('User', UsersSchema);