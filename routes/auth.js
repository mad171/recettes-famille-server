const jwt = require('express-jwt');
const config = require('../config.js');

/** Get Token from a request header including a token */
const getTokenFromHeaders = (req) => {
    console.log('[ROUTER AUTH]: GetTokenFromHeaders');
    const { headers: { authorization } } = req;

    if (authorization && authorization.split(' ')[0] === 'Token') {
        return authorization.split(' ')[1];
    }
    return null;
};

/** Authorizatoin formating */
const auth = {
    required: jwt({
        secret: config.secret,
        userProperty: 'payload',
        getToken: getTokenFromHeaders,
    }),
    optional: jwt({
        secret: config.secret,
        userProperty: 'payload',
        getToken: getTokenFromHeaders,
        credentialsRequired: false,
    }),
};

module.exports = auth;