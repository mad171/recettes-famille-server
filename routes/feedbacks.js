/** ROUTER MANAGING REQUESTS ABOUT RECIPES */

let express = require('express');
let router = express.Router();
let Feedback = require('../models/Feedback.js');
const Users = require('../models/User.js');
const auth = require('./auth');


// ______________________________________________________________ \\
// ____________________________ GETS ____________________________ \\

/** GET ALL FEEDBACKS of a given category */
router.get('/all/:category', auth.required, function (req, res, next) {
    console.log('[ROUTER-FEEDBACKS]: Request GET feedbacks (by category --> ' + req.params.category + ')');
    Feedback.find({ 'category': req.params.category }, function (err, post) {
        if (err) return next(err);
        res.json(post);
    });
});


// ______________________________________________________________ \\
// ___________________________ POSTS ____________________________ \\

/** SAVE FEEDBACK */
router.post('/add', auth.required, function (req, res, next) {
    console.log('[ROUTER-FEEDBACKS]: Request POST save a new feedback');

    const newFeedback = req.body;

    // Find editor's nickname and add it to the new recipe
    const { payload: { id } } = req;
    Users.findById(id)
        .then((user) => {
            if (!user) {
                return res.sendStatus(401);
            }
            newFeedback.nickname = user.nickname;

            // Générate a new id (= max des Id + 1)
            Feedback.aggregate([
                { $group: { _id: null, feedback_id: { $max: '$feedback_id' } } },
                { $project: { _id: 0, feedback_id: 1 } }
            ]).
                then(function (response) {
                    if (response[0]) {
                        newFeedback.feedback_id = response[0].feedback_id + 1;
                    }
                    else {
                        newFeedback.feedback_id = 1;
                    }

                    // Create the new recipe
                    Feedback.create(newFeedback, function (err, post) {
                        if (err) return next(err);
                        res.json(post);
                    });
                });

        });
});


// ______________________________________________________________ \\
// ____________________________ PUTS ____________________________ \\


/** DELETE RECIPE provided you are its author or an admin*/
router.delete('/delete/:feedback_id', auth.required, function (req, res, next) {
    console.log('[ROUTER-FEEDBACKS]: Request DELETE a feedback (id --> ' + req.params.feedback_id + ')');

    const { payload: { id } } = req;

    Users.findById(id)
        .then((user) => {
            if (!user) { return res.sendStatus(401); }
            // Find the recipe to delete
            Feedback.findOne({ 'feedback_id': req.params.feedback_id }, function (err, feedbackToDelete) {
                if (err) { return next(err); }
                //Check the rights to edit the recipe
                else if (user.isAdmin === true || user.nickname === feedbackToDelete.nickname) {
                    feedbackToDelete.remove(function (err, deletedFeedback) {
                        if (err) { return next(err); }
                        res.send(deletedFeedback);
                    });
                }
                else {
                    return res.status(403).json({
                        errors: {
                            editor: 'has not the rights to delete this feedback',
                        },
                    });
                }
            });
        });
});

module.exports = router;

