/** ROUTER MANAGING REQUESTS ABOUT USERS */

const passport = require('passport');
const router = require('express').Router();
const auth = require('./auth');
const Users = require('../models/User.js');


// ______________________________________________________________ \\
// ____________________________ GETS ____________________________ \\

/** GET the user's PROFILE (protected) */
router.get('/profile', auth.required, (req, res, next) => {
  const { payload: { id } } = req;

  return Users.findById(id)
    .then((user) => {
      if (!user) {
        return res.sendStatus(400);
      }
      return res.json({ user: user.toAuthJSON() });
    });
});


// ______________________________________________________________ \\
// ___________________________ POSTS ____________________________ \\

/** REGISTER a new user (not protected) */
router.post('/register', auth.optional, (req, res, next) => {
  const { body: { user } } = req;

  user.isAdmin = false;

  if (!user.nickname) {
    return res.status(422).json({
      errors: {
        nickname: 'is required',
      },
    });
  }
  if (!user.password) {
    return res.status(422).json({
      errors: {
        password: 'is required',
      },
    });
  }

  const finalUser = new Users(user);
  finalUser.setPassword(user.password);

  // Check that the nickname has not already been used
  Users.count({ nickname: user.nickname }, (err, count) => {
    if (err) {
      return next(err);
    }
    else if (count > 0) {
      return res.status(422).json({
        errors: {
          nickname: 'is already used',
        },
      });
    }
    else {
      return finalUser.save()
        .then(() => res.json({ user: finalUser.toAuthJSON() }));
    }
  }
  );

});

/** LOGIN request (not protected) */
router.post('/login', (req, res, next) => {
  const { body: { user } } = req;

  if (!user.nickname) {
    return res.status(422).json({
      errors: {
        nickname: 'is required',
      },
    });
  }

  if (!user.password) {
    return res.status(422).json({
      errors: {
        password: 'is required',
      },
    });
  }

  return passport.authenticate('local', { session: false }, (err, passportUser, info) => {
    if (err) {
      return res.status(422).json({
        errors: {
          nickname: 'maybe wrong',
          password: 'maybe wrong',
        },
      });
    }
    if (passportUser) {
      const user = passportUser;
      user.token = passportUser.generateJWT();
      return res.json({ user: user.toAuthJSON() });
    }
    return status(400).info;
  })(req, res, next);

});


// ______________________________________________________________ \\
// ___________________________ PUTS ____________________________ \\

/** PROMOTE a user TO ADMIN RANK (protected) */
router.put('/promote', auth.required, (req, res, next) => {

  const { payload: { id } } = req;
  const { body: userToUpgrade } = req;

  if (!userToUpgrade.nickname) {
    return res.status(422).json({
      errors: {
        nickname: 'You should give the name of the user to promote',
      },
    });
  }

  Users.findById(id)
    .then((user) => {
      if (!user) {
        return res.sendStatus(400);
      }
      else if (user.isAdmin === true) {
        Users.findOneAndUpdate({ 'nickname': userToUpgrade.nickname }, { isAdmin: true }, function (err, post) {
          if (err) return next(err);
          res.json(post);
        });
      }
      else {
        return res.status(422).json({
          errors: {
            admin: 'you are not an admin'
          },
        });
      }
    });

});

module.exports = router;

